/**
 * @file
 * Default "header" portion of the Quantcast tracker code.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.gatsby_preview_sidebar = {
    attach: function (context, settings) {
      // This code comes from directly from Quantcast.
      var _qevents = _qevents || [];
      (function() {
        var elem = document.createElement('script');
        elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
        elem.async = true;
        elem.type = "text/javascript";
        var scpt = document.getElementsByTagName('script')[0];
        scpt.parentNode.insertBefore(elem, scpt);
      })();

      // This line was modified to use the Drupal settings variable.
      _qevents.push(drupalSettings.quantcast.pcode);
    }
  };

})(jQuery, Drupal);
