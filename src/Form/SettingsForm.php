<?php

namespace Drupal\quantcast\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the configuration export form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quantcast_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['quantcast.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('quantcast.settings');

    $form['pcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('P-Code'),
      '#description' => $this->t('If you do not already have a Quantcast P-Code, please acquire it by registering at Quantcast.com.'),
      '#default_value' => $settings->get('pcode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('quantcast.settings');

    $settings->set('pcode', $form_state->getValue('pcode'));

    $settings->save();
    parent::submitForm($form, $form_state);
  }

}
